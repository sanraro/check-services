package main

import (
  "io/ioutil"
  "time"
  "net" 
  "net/http"
  "strings"
  "strconv"
)

// Invoke service request
func DoRequest(reqType string, srvUrl string, headers map[string]string, data string, timeout int) (int, []byte, map[string][]string, error) {
  // default timeout of 2 seconds
  if (timeout == 0) {
    timeout = 2
  }
  timeoutSeconds := time.Duration(timeout) * time.Second
  tr := &http.Transport{
    MaxIdleConns:       10,
    IdleConnTimeout:    timeoutSeconds,
    DisableCompression: true,
    Dial: (&net.Dialer{
      Timeout: timeoutSeconds,
    }).Dial,
    TLSHandshakeTimeout: timeoutSeconds,
  }
  client := &http.Client{
    Transport: tr,
    Timeout: timeoutSeconds,
  }

  req, err := http.NewRequest(reqType, srvUrl, strings.NewReader(data))
  if err != nil {
    return 0, nil, nil, err
  }
  // If headers has been sent, use it
  if headers != nil {
    for key, value := range headers {
      req.Header.Add(key, value)
    }
  }
  // If method used is POST, send good headers
  if (reqType == "POST") {
    req.Header.Add("Content-Type", "application/json")
    req.Header.Add("Content-Length", strconv.Itoa(len(data)))
  }
  resp, err := client.Do(req)
  if err != nil {
    return 0, nil, nil, err
  }

  // Close connection when read all body
  defer resp.Body.Close()
  body, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    return 0, nil, nil, err
  }
  return resp.StatusCode, body, resp.Header, err
}
