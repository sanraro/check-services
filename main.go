package main

import (
  "github.com/robfig/cron"
  "os"
  "os/signal"
)

func main() {
  config := ReadConfig("config.yaml")

  // Start health service
  go HealthCheckServer(config.Health)
  // Start check services
  c := cron.New()
  c.AddFunc("*/10 * * * * *", func() { CheckAllServices(config.Manager, config.Checks) })
  c.Start()
  sig := make(chan os.Signal)
  signal.Notify(sig, os.Interrupt, os.Kill)
  <-sig
}
