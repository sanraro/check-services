package main

import (
  "fmt"
  "github.com/robfig/cron"
  "os"
  "os/signal"
)

func main() {
  args := os.Args
  if len(args) == 1 {
    fmt.Println("\n   syntax: check-services yaml_config_file\n\n")
  } else {
    config := ReadConfig(args[1])

    // Start health service
    go HealthCheckServer(config.Health)
    // Start check services
    c := cron.New()
    c.AddFunc("*/60 * * * * *", func() { CheckAllServices(config.Manager, config.Checks) })
    c.Start()
    sig := make(chan os.Signal)
    signal.Notify(sig, os.Interrupt, os.Kill)
    <-sig
  }
}
