package main

import (
  "fmt"
  "log"
  "strings"
)

// Check service
func CheckService(name string, request Request, response Response) (error) {
  // Check each of element
  statusCode, body, header, err := DoRequest(request.Method, request.Endpoint, nil, request.Body, request.Timeout)
  if err == nil {
    // Check response code
    if (response.Code != 0 && response.Code != statusCode) {
      return fmt.Errorf("Wrong status code: got %d, expected %d", statusCode, response.Code)
    }
    // Check headers
    headkey := response.Header.Key
    if (headkey != "") {
      notFound := true
      for _, val := range header[headkey] {
        if val == response.Header.Value {
	  notFound = false 
	}
      }
      if (notFound) {
        return fmt.Errorf("Wrong value for header '%s': got '%s', expected '%s'", headkey, header[headkey], response.Header.Value)
      } 
    }
    // Check body
    if (response.Body != "" && !strings.Contains(string(body), response.Body)) {
      return fmt.Errorf("Text '%s' not found in body", response.Body)
    }
  }

  return err 
}

// Notity service failure to manager
func notifyError(manager Manager, service string, msg string) {
  if (manager.Endpoint != "") {
    log.Printf(" Sending error message for service '%s'", service)
    headParams := map[string]string{"Authorization": "Bearer " + manager.Bearer}
    jsonString := "{\"service\": \"" + service + "\", \"description\": \"" + msg + "\"}"

    statusCode, body, header, err := DoRequest("POST", manager.Endpoint, headParams, jsonString, 0)
    if (err != nil) {
      log.Printf(" Error sending message to manager:  #%v ", err)
    }

    _ = statusCode
    _ = body
    _ = header
  }
}

// Check all defined services and notify failures
func CheckAllServices(manager Manager, checks []Check) {
  log.Printf(" --- Starting services check\n")
  for _, elem := range checks {
    err := CheckService(elem.Service, elem.Request, elem.Response)
    if err != nil {
      log.Printf(" Error with service %s:  #%v ", elem.Service, err)
      notifyError(manager, elem.Service, err.Error())
    }
  }
}
