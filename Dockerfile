# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Download and install the latest release of dep
ADD https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 /usr/bin/dep
RUN chmod +x /usr/bin/dep

# Copy the local package files to the container's workspace.
ADD . /go/src/github.com/sramos/check-services
# Copy the code from the host and compile it
WORKDIR $GOPATH/src/github.com/sramos/check-services
COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure --vendor-only
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /check-services .

ENTRYPOINT ["/check-services"]

# Document that the service listens on port 8080.
EXPOSE 8001
