package main

import (
  "net/http"
  "fmt"
  "log"
)

// Open web service to allow probe check health
func HealthCheckServer(health Health) {
  log.Printf(" --- Starting healtCheck service")
  http.HandleFunc(health.Endpoint, func(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Name", health.Name)
    w.WriteHeader(200)
    fmt.Fprintf(w, "Pong")
  })
  err := http.ListenAndServe(health.Listen, nil)
  if err != nil {
    log.Fatalf("Unable to load HealthService: %v", err)
  }
}
