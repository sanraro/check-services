package main

import (
  "log"
  "io/ioutil"
  "gopkg.in/yaml.v2"
)

// Structs to store YAML config file 
type Request struct {
  Endpoint  string   `yaml:"endpoint"`
  Method    string   `yaml:"method"`
  Body      string   `yaml:"body"`
  Timeout   int      `yaml:"timeout"`
}
type Response struct {
  Code      int      `yaml:"code"`
  Header    Header   `yaml:"header"`
  Body      string   `yaml:"body"`
}
type Header struct {
  Key       string   `yaml:"key"`
  Value     string   `yaml:"value"`
}
type Check struct {
  Service   string   `yaml:"service"`
  Request   Request  `yaml:"request"`
  Response  Response `yaml:"response"`
}
type Health struct {
  Listen    string   `yaml:"listen"`
  Endpoint  string   `yaml:"endpoint"`
  Name      string   `yaml:"name"`
}
type Manager struct {
  Endpoint  string   `yaml:"endpoint"`
  Method    string   `yaml:"method"`
  Bearer    string   `yaml:"bearer"`
}
type Config struct {
  Manager Manager    `yaml:"manager"`
  Health  Health     `yaml:"healthservice"`
  Checks []Check     `yaml:"checks"`
}

func ReadConfig(filename string)(Config) {
  var config Config

  // Open file to read
  yamlFile, err := ioutil.ReadFile(filename)
  if err != nil {
    log.Fatalf("yamlFile.Get err   #%v ", err)
  }

  // Parse yaml file
  err = yaml.Unmarshal(yamlFile, &config)
  if err != nil {
    log.Fatalf("Unmarshal: %v", err)
  }

  return config
}
