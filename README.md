# check-services

`check-services` is a web services testing tool developed to see how is to programing with go.

## Instalation

```
  $ go get -u github.com/sramos/check-services
```

To install dependencies:

```
  $ go get gopkg.in/yaml.v2
  $ go get github.com/robofig/cron
```

## Docker

Build image:

```
  $ docker build -t check-services .
```

Run image as container:

```
  $ docker run -i -t check-services config.yaml
```

## Configure

Create a config.yaml file with manager and services definition.

For failure service notification, change bearer client id:

```
manager:
  endpoint: "https://interview-notifier-svc.spotahome.net/api/v1/notification"
  bearer: 12345678901234567890123456789
```

Update probe name, port and endpoint for health service:

```
healthservice:
  listen: ":8001"
  endpoint: "/ping"
  name: "check01"
```

Define services to check:

```
checks:
  - service: google
    request:
      endpoint: "https://google.es/"
      method: GET
      timeout: 2
    response:
      code: 200
      header:
        key: Content-Type
        value: "text/html; charset=UTF-8"
      body: "Google"
  - service: crta.me
    request:
      endpoint: "http://crta.me/stored_urls"
      method: POST
      body: "{\"action\": \"foo\", \"name\": \"bar\"}"
      timeout: 2
    response:
      code: 422
  - service: otherapp-euwest1
    request:
      endpoint: "https://euwest1.otherapp.io/checks"
      method: POST
      body: "{\"checker\": \"potahome\"}"
      timeout: 2
    response:
      header:
        key: healthcheck
        value: ok
```

## Usage

```
  $ go run *.go config.yaml
```

## To-do

* Take bearer id from env variable
* Optimize docker image
* Enable services auto provisioning (recive servers to ckeck from manager API)
* Notify status changes and ignore repetition failures.
* Enable to adjust own monitoring schedule for each service.
